TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        case.c \
        chain.c \
        goban.c \
        list.c \
        main.c \
        windows_console_handler.c

HEADERS += \
    case.h \
    chain.h \
    constants.h \
    goban.h \
    list.h \
    windows_console_handler.h
