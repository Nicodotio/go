#include "case.h"
#include "chain.h"
#include "constants.h"

/**
 * @brief check_liberty How many empty cases there is around the x y case.
 * @param goban The goban of the game.
 * @param x x of the case to check.
 * @param y y of the case to check.
 * @return Number of empty cases around this case.
 */
int check_liberty(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y)
{
    int liberty = 4;

    if (x == GOBAN_SIZE - 1 || goban[x + 1][y] != EMPTY_CASE) {
        liberty--;
    }
    if (x == 0 || goban[x - 1][y] != EMPTY_CASE) {
        liberty--;
    }
    if (y == GOBAN_SIZE - 1 || goban[x][y + 1] != EMPTY_CASE) {
        liberty--;
    }
    if (y == 0 || goban[x][y - 1] != EMPTY_CASE) {
        liberty--;
    }

    return liberty;
}

/**
 * @brief check_is_territory
 * @param goban The goban of the game.
 * @param x x of the case to check.
 * @param y y of the case to check.
 * @return The color of the territory.
 */
char check_is_territory(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y)
{
    char color = 'U';
    char previous_color = 'U';

    if (x < GOBAN_SIZE - 1 && goban[x + 1][y] != EMPTY_CASE) {
        previous_color = color = goban[x + 1][y];
    }
    if (x != 0 && goban[x - 1][y] != EMPTY_CASE) {
        color = goban[x - 1][y];
        if (previous_color != 'U' && previous_color != color) {
            return 'N';
        } else {
            previous_color = color;
        }
    }
    if (y < GOBAN_SIZE - 1 && goban[x][y + 1] != EMPTY_CASE) {
        color = goban[x][y + 1];
        if (previous_color != 'U' && previous_color != color) {
            return 'N';
        } else {
            previous_color = color;
        }
    }
    if (y != 0 && goban[x][y - 1] != EMPTY_CASE) {
        color = goban[x][y - 1];
        if (previous_color != 'U' && previous_color != color) {
            return 'N';
        } else {
            previous_color = color;
        }
    }
    return color;
}

/**
 * @brief is_case_playable
 * @param goban The goban of the game.
 * @param x x of the case to check.
 * @param y y of the case to check.
 * @param player The current player.
 * @param old_goban_a The previous goban of player A.
 * @param old_goban_b The previous goban of player B.
 * @return True if the case is playable.
 */
int is_case_playable(char goban[GOBAN_SIZE][GOBAN_SIZE],
                     int x,
                     int y,
                     char player,
                     char old_goban_a[GOBAN_SIZE][GOBAN_SIZE],
                     char old_goban_b[GOBAN_SIZE][GOBAN_SIZE])
{
    char goban_test[GOBAN_SIZE][GOBAN_SIZE];
    int is_playable;

    copy_goban(goban_test, goban);

    if (goban[x][y] != EMPTY_CASE) {
        is_playable = 0;
    } else {
        goban_test[x][y] = player;
        if (atari(goban_test, x, y, player)) {
            is_playable = 1;
        } else if (chain_handler(goban_test, x, y, player)) {
            is_playable = 0;
        } else {
            is_playable = 1;
        }
    }

    if (is_playable) {
        if (player == 'A') {
            is_playable = !are_goban_equal(old_goban_a, goban_test);
            if (is_playable) {
                copy_goban(old_goban_a, goban_test);
            }
        } else {
            is_playable = !are_goban_equal(old_goban_b, goban_test);
            if (is_playable) {
                copy_goban(old_goban_b, goban_test);
            }
        }
    }

    return is_playable;
}

/**
 * @brief remove_stone Put an EMPTY_CASE at x y.
 * @param goban The goban of the game.
 * @param x x of the case to check.
 * @param y y of the case to check.
 */
void remove_stone(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y)
{
    goban[x][y] = EMPTY_CASE;
}
