#ifndef CASE_H
#define CASE_H

#include "goban.h"

int check_liberty(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y);

char check_is_territory(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y);

int is_case_playable(char goban[GOBAN_SIZE][GOBAN_SIZE],
                     int x,
                     int y,
                     char player,
                     char old_goban_a[GOBAN_SIZE][GOBAN_SIZE],
                     char old_goban_b[GOBAN_SIZE][GOBAN_SIZE]);

void remove_stone(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y);

#endif // CASE_H
