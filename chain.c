#include <stdlib.h>

#include "case.h"
#include "chain.h"

/**
 * @brief search_chain Search a chain.
 * @param goban The goban of the game.
 * @param x x of the case to check.
 * @param y y of the case to check.
 * @param searching The element to be part of the chain.
 * @param chain the list where to add element found.
 */
void search_chain(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y, char searching, list *chain)
{
    if (x < GOBAN_SIZE - 1 && goban[x + 1][y] == searching) {
        if (!contain_element(chain, x + 1, y)) {
            list_insert(chain, x + 1, y);
            search_chain(goban, x + 1, y, searching, chain);
        }
    }
    if (x != 0 && goban[x - 1][y] == searching) {
        if (!contain_element(chain, x - 1, y)) {
            list_insert(chain, x - 1, y);
            search_chain(goban, x - 1, y, searching, chain);
        }
    }
    if (y < GOBAN_SIZE - 1 && goban[x][y + 1] == searching) {
        if (!contain_element(chain, x, y + 1)) {
            list_insert(chain, x, y + 1);
            search_chain(goban, x, y + 1, searching, chain);
        }
    }
    if (y != 0 && goban[x][y - 1] == searching) {
        if (!contain_element(chain, x, y - 1)) {
            list_insert(chain, x, y - 1);
            search_chain(goban, x, y - 1, searching, chain);
        }
    }
}

/**
 * @brief chain_handler Take care of finding and removing chain.
 * @param goban The goban of the game.
 * @param x x of the case to check.
 * @param y y of the case to check.
 * @param searching The element to be part of the chain.
 * @return True if there is an atari.
 */
int chain_handler(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y, char searching)
{
    list *chain = list_create();

    list_insert(chain, x, y);
    search_chain(goban, x, y, searching, chain);

    // Check liberties of each element of the list.
    node *iterator = chain->first;
    int is_atari = 0;
    while (iterator != NULL) {
        if (check_liberty(goban, iterator->x, iterator->y) != 0) {
            is_atari = 0;
            break;
        } else {
            is_atari = 1;
        }
        iterator = iterator->next;
    }

    iterator = chain->first;
    if (is_atari) {
        // If no liberties for the chain -> remove the chain.
        while (iterator != NULL) {
            remove_stone(goban, iterator->x, iterator->y);
            iterator = iterator->next;
        }
    }
    list_destroy(chain);
    return is_atari;
}

/**
 * @brief atari Take care of atari.
 * @param goban The goban of the game.
 * @param x x of the case to check.
 * @param y y of the case to check.
 * @param player The current player.
 * @return The number of atari.
 */
int atari(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y, char player)
{
    char opponent;
    int number_of_attari = 0;
    if (player == 'A') {
        opponent = 'B';
    } else {
        opponent = 'A';
    }
    if (x < GOBAN_SIZE - 1 && goban[x + 1][y] == opponent) {
        if (chain_handler(goban, x + 1, y, opponent)) {
            number_of_attari++;
        }
    }
    if (x != 0 && goban[x - 1][y] == opponent) {
        if (chain_handler(goban, x - 1, y, opponent)) {
            number_of_attari++;
        }
    }
    if (y < GOBAN_SIZE - 1 && goban[x][y + 1] == opponent) {
        if (chain_handler(goban, x, y + 1, opponent)) {
            number_of_attari++;
        }
    }
    if (y != 0 && goban[x][y - 1] == opponent) {
        if (chain_handler(goban, x, y - 1, opponent)) {
            number_of_attari++;
        }
    }
    return number_of_attari;
}
