#ifndef CHAIN_H
#define CHAIN_H

#include "constants.h"
#include "goban.h"
#include "list.h"

void search_chain(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y, char searching, list *chain);

int chain_handler(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y, char searching);

int atari(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y, char player);

#endif // CHAIN_H
