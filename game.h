#ifndef GAME_H
#define GAME_H

#include "constants.h"
#include "goban.h"

int atari(char goban[GOBAN_SIZE][GOBAN_SIZE], int x, int y, char player);

int play(char goban[GOBAN_SIZE][GOBAN_SIZE],
         char player,
         char old_goban_a[GOBAN_SIZE][GOBAN_SIZE],
         char old_goban_b[GOBAN_SIZE][GOBAN_SIZE]);

scores_t count_points(char goban[GOBAN_SIZE][GOBAN_SIZE]);

#endif // GAME_H
