#include <stdio.h>

#include "constants.h"
#include "goban.h"

/**
 * @brief init_goban Initialize goban with EMPTY_CASE
 * @param goban The goban to initialize.
 */
void init_goban(char goban[GOBAN_SIZE][GOBAN_SIZE])
{
    for (int line = 0; line < GOBAN_SIZE; line++) {
        for (int column = 0; column < GOBAN_SIZE; column++) {
            goban[line][column] = EMPTY_CASE;
        }
    }
}

/**
 * @brief copy_goban Copy goban_src in goban_dest.
 * @param goban_dest The destination goban.
 * @param goban_src The copied goban.
 */
void copy_goban(char goban_dest[GOBAN_SIZE][GOBAN_SIZE], char goban_src[GOBAN_SIZE][GOBAN_SIZE])
{
    for (int i = 0; i < GOBAN_SIZE; i++) {
        for (int j = 0; j < GOBAN_SIZE; j++) {
            goban_dest[i][j] = goban_src[i][j];
        }
    }
}

/**
 * @brief are_goban_equal Check if two gobans are equal.
 * @param goban1 First goban.
 * @param goban2 Second goban.
 * @return True if gobans are equals.
 */
int are_goban_equal(char goban1[GOBAN_SIZE][GOBAN_SIZE], char goban2[GOBAN_SIZE][GOBAN_SIZE])
{
    for (int i = 0; i < GOBAN_SIZE; i++) {
        for (int j = 0; j < GOBAN_SIZE; j++) {
            if (goban1[i][j] != goban2[i][j]) {
                return 0;
            }
        }
    }
    return 1;
}
