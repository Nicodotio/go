#ifndef GOBAN_H
#define GOBAN_H

#define GOBAN_SIZE 19

void init_goban(char goban[GOBAN_SIZE][GOBAN_SIZE]);

void copy_goban(char goban_dest[GOBAN_SIZE][GOBAN_SIZE], char goban_src[GOBAN_SIZE][GOBAN_SIZE]);

int are_goban_equal(char goban1[GOBAN_SIZE][GOBAN_SIZE], char goban2[GOBAN_SIZE][GOBAN_SIZE]);

#endif // GOBAN_H
