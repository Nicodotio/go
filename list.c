#include <stdlib.h>

#include "list.h"

/**
 * @brief list_create Create a new list.
 * @return An initialized list.
 */
list *list_create()
{
    list *new_list;
    new_list = (list *) malloc(sizeof(list));
    new_list->first = NULL;
    new_list->last = NULL;
    new_list->count = 0;

    return new_list;
}

/**
 * @brief list_insert Insert an element.
 * @param list The list where to insert the element.
 * @param x The x data to insert.
 * @param y The y data to insert.
 */
void list_insert(list *list, int x, int y)
{
    node *new_node;
    new_node = (node *) malloc(sizeof(node));
    new_node->x = x;
    new_node->y = y;
    new_node->next = NULL;

    if (list->first == NULL) {
        list->first = new_node;
    }
    if (list->last != NULL) {
        list->last->next = new_node;
    }
    list->last = new_node;
    list->count++;
}

/**
 * @brief copy_list Copy list_src in list_dest.
 * @param list_dest The destination list.
 * @param list_src The copied list.
 */
void copy_list(list *list_dest, list *list_src)
{
    node *temp = list_src->first;
    while (temp != NULL) {
        list_insert(list_dest, temp->x, temp->y);
        temp = temp->next;
    }
}

/**
 * @brief contain_element Check if element x y is in list.
 * @param list The list where to search the element.
 * @param x x data.
 * @param y y data.
 * @return True if found.
 */
int contain_element(list *list, int x, int y)
{
    node *temp = list->first;
    if (temp != NULL) {
        while (temp != NULL) {
            if (temp->x == x && temp->y == y) {
                return 1;
            }
            temp = temp->next;
        }
    }
    return 0;
}

/**
 * @brief list_destroy Destroy a list.
 * @param list The list to destroy.
 */
void list_destroy(list *list)
{
    node *temp = list->first;
    if (list->first != NULL) {
        while (temp->next != NULL) {
            node *next = temp->next;
            free(temp);
            temp = next;
        }
    }
    free(list);
}
