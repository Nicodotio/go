#ifndef LIST_H
#define LIST_H

typedef struct node_struct
{
    int x, y;
    struct node_struct *next;
} node;

typedef struct
{
    node *first;
    node *last;
    int count;
} list;

list *list_create(void);

void list_insert(list *list, int x, int y);

void copy_list(list *list_dest, list *list_src);

int contain_element(list *list, int x, int y);

void list_destroy(list *list);

#endif // LIST_H
