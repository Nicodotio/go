#include <stdio.h>
#include <windows.h>

#include "case.h"
#include "chain.h"
#include "constants.h"
#include "goban.h"
#include "list.h"
#include "windows_console_handler.h"

typedef struct
{
    float score_player_a;
    float score_player_b;
} scores;

/**
 * @brief change_color Change background color depending on the player.
 * @param character The character representing the player.
 */
void change_color(char character)
{
    if (character == 'A') {
        set_background_color(COLOR_BLACK);
    } else if (character == 'B') {
        set_background_color(COLOR_WHITE);
    }
}

/**
 * @brief show_goban Display goban in console.
 * @param goban The goban to display.
 */
void show_goban(char goban[GOBAN_SIZE][GOBAN_SIZE])
{
    printf("    ");
    for (int i = 0; i < GOBAN_SIZE; i++) {
        printf("%c ", 'A' + i);
    }
    printf("\n   ");

    for (int column = 0; column < GOBAN_SIZE; column++) {
        printf("+-");
    }
    printf("+\n");
    for (int line = 0; line < GOBAN_SIZE; line++) {
        printf("%2i |", line + 1);

        for (int column = 0; column < GOBAN_SIZE; column++) {
            change_color(goban[line][column]);
            printf("%c", goban[line][column]);
            set_background_color(DEFAULT_BACKGROUND_COLOR);
            printf("|");
        }
        printf("\n   ");
        for (int column = 0; column < GOBAN_SIZE; column++) {
            printf("+-");
        }
        printf("+\n");
    }
}

/**
 * @brief play For a player turn.
 * @param goban The goban of the game.
 * @param player The current player.
 * @param old_goban_a The previous goban of player A.
 * @param old_goban_b The previous goban of player B.
 * @return True if player has play, false if he passed.
 */
int play(char goban[GOBAN_SIZE][GOBAN_SIZE],
         char player,
         char old_goban_a[GOBAN_SIZE][GOBAN_SIZE],
         char old_goban_b[GOBAN_SIZE][GOBAN_SIZE])
{
    int line, column;
    int is_valid;
    int y_offset = 0;
    do {
        console_handler_init();
        show_goban(goban);
        printf("Turn of player ");
        change_color(player);
        printf("%c", player);
        set_background_color(DEFAULT_BACKGROUND_COLOR);
        printf("\n");
        printf("Click where you want to play or click HERE to pass your turn.\n");
        COORD coords;
        do {
            coords = get_click_location();
            coords.Y -= y_offset;
            // Location of "HERE" to pass your turn.
            if (coords.Y == GOBAN_SIZE * 2 + 3 && coords.X > 37 && coords.X < 42) {
                system("CLS");
                return 0;
            }
        } while ((coords.X == -1 && coords.Y == -1) || coords.X < 2 || coords.X > 40
                 || coords.Y > 38);

        column = (coords.X - 4) / 2;
        line = (coords.Y - 2) / 2;

        is_valid = is_case_playable(goban, line, column, player, old_goban_a, old_goban_b);
        if (!is_valid) {
            system("CLS");
            printf("INVALID! TRY AGAIN!\n");
            y_offset = 1;
        }
    } while (!is_valid);
    system("CLS");
    goban[line][column] = player;
    atari(goban, line, column, player);
    return 1;
}

/**
 * @brief count_points Counts each player's points
 * @param goban The goban of the game.
 * @return A struct with each player's score.
 */
scores count_points(char goban[GOBAN_SIZE][GOBAN_SIZE])
{
    scores scores;
    scores.score_player_a = 0;
    scores.score_player_b = 0;

    for (int i = 0; i < GOBAN_SIZE; i++) {
        for (int j = 0; j < GOBAN_SIZE; j++) {
            if (goban[i][j] == 'A') {
                scores.score_player_a++;
            } else if (goban[i][j] == 'B') {
                scores.score_player_b++;
            }
        }
    }
    list *exclude = list_create();

    for (int i = 0; i < GOBAN_SIZE; i++) {
        for (int j = 0; j < GOBAN_SIZE; j++) {
            if (goban[i][j] == EMPTY_CASE && !contain_element(exclude, i, j)) {
                list *chain = list_create();
                list_insert(chain, i, j);
                search_chain(goban, i, j, EMPTY_CASE, chain);

                node *iterator = chain->first;
                int is_territory = 0;
                // Unknow color for the moment.
                char color = 'U';
                while (iterator != NULL) {
                    char new_color = check_is_territory(goban, iterator->x, iterator->y);
                    if (new_color != 'N') {
                        // Color == N -> mixed colors -> not a territory.
                        if (new_color != 'U') {
                            if (new_color != color && color != 'U') {
                                is_territory = 0;
                                break;
                            } else {
                                is_territory = 1;
                                color = new_color;
                            }
                        }
                        iterator = iterator->next;
                    } else {
                        is_territory = 0;
                        break;
                    }
                }
                copy_list(exclude, chain);
                if (is_territory) {
                    if (color == 'A') {
                        scores.score_player_a += (float) chain->count;
                    } else {
                        scores.score_player_b += (float) chain->count;
                    }
                }
                list_destroy(chain);
            }
        }
    }

    list_destroy(exclude);

    scores.score_player_a += (float) 7.5;

    return scores;
}

int main()
{
    ShowWindow(GetConsoleWindow(), SW_MAXIMIZE);
    system("TITLE Go");
    system("COLOR 27");
    char goban[GOBAN_SIZE][GOBAN_SIZE];
    init_goban(goban);
    char old_goban_a[GOBAN_SIZE][GOBAN_SIZE];
    init_goban(old_goban_a);
    char old_goban_b[GOBAN_SIZE][GOBAN_SIZE];
    init_goban(old_goban_b);
    int a_has_play = 1, b_has_play = 1;
    do {
        a_has_play = play(goban, 'A', old_goban_a, old_goban_b);
        if (b_has_play || a_has_play) {
            b_has_play = play(goban, 'B', old_goban_a, old_goban_b);
        }
    } while (a_has_play || b_has_play);
    scores scores = count_points(goban);
    show_goban(goban);

    if (scores.score_player_a > scores.score_player_b) {
        printf("The winner is black (%0.1f VS %0.1f).\n",
               (double) scores.score_player_a,
               (double) scores.score_player_b);
    } else {
        printf("The winner is white (%0.1f VS %0.1f).\n",
               (double) scores.score_player_b,
               (double) scores.score_player_a);
    }
    reset_console_mode();
    return 0;
}
