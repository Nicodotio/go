#include <stdio.h>
#include <windows.h>

static HANDLE hStdin;
static DWORD fdwSaveOldMode;

/**
 * @brief error_exit Exit after an error.
 * @param lpszMessage The error message.
 */
void error_exit(LPSTR lpszMessage)
{
    fprintf(stderr, "%s\n", lpszMessage);

    // Restore input mode on exit.
    SetConsoleMode(hStdin, fdwSaveOldMode);

    ExitProcess(1);
}

/**
 * @brief reset_console_mode Reset console mode.
 */
void reset_console_mode()
{
    SetConsoleMode(hStdin, fdwSaveOldMode);
}

/**
 * @brief console_handler_init Initialize the console handler.
 */
void console_handler_init()
{
    DWORD fdwMode;

    // Get the standard input handle.
    hStdin = GetStdHandle(STD_INPUT_HANDLE);
    if (hStdin == INVALID_HANDLE_VALUE)
        error_exit("GetStdHandle");

    // Save the current input mode, to be restored on exit.
    if (!GetConsoleMode(hStdin, &fdwSaveOldMode))
        error_exit("GetConsoleMode");

    fdwMode = ENABLE_EXTENDED_FLAGS;
    if (!SetConsoleMode(hStdin, fdwMode))
        error_exit("SetConsoleMode");

    // Enable mouse input events.
    fdwMode = ENABLE_MOUSE_INPUT;
    if (!SetConsoleMode(hStdin, fdwMode))
        error_exit("SetConsoleMode");
}

/**
 * @brief get_click_location Get cursor location of the click.
 * @return The location of the cursor after a click.
 */
COORD get_click_location()
{
    DWORD cNumRead;
    INPUT_RECORD irInBuf[1];
    COORD coords;

    coords.X = -1;
    coords.Y = -1;

    if (!ReadConsoleInput(hStdin,     // input buffer handle
                          irInBuf,    // buffer to read into
                          1,          // size of read buffer
                          &cNumRead)) // number of records read
        error_exit("ReadConsoleInput");

    if (irInBuf[0].EventType == MOUSE_EVENT && irInBuf[0].Event.MouseEvent.dwEventFlags == 0
        && irInBuf[0].Event.MouseEvent.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED) {
        coords.X = irInBuf[0].Event.MouseEvent.dwMousePosition.X;
        coords.Y = irInBuf[0].Event.MouseEvent.dwMousePosition.Y;
    }
    return coords;
}

/**
 * @brief set_background_color Change background color of the console.
 * @param color The color to set.
 */
void set_background_color(unsigned short color)
{
    HANDLE hcon = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hcon, color);
}
