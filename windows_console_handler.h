#ifndef WINDOWS_CONSOLE_HANDLER_H
#define WINDOWS_CONSOLE_HANDLER_H

#include <windows.h>

void console_handler_init(void);

COORD get_click_location(void);

void error_exit(LPSTR lpszMessage);

void reset_console_mode(void);

void set_background_color(unsigned short color);

#endif // WINDOWS_CONSOLE_HANDLER_H
